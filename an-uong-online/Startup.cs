﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(an_uong_online.Startup))]
namespace an_uong_online
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
